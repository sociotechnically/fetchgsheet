
FetchGSheet is an example of how to use a short python script to download a Google Sheets document.

I wrote a tutorial on how to use it: https://kayleachampion.com/2021/11/12/tutorial-easy-script-to-download-google-sheets/

I adapted this script from an example shared with me by Benjamin Mako Hill -- https://mako.cc/


