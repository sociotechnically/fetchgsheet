#!/usr/bin/env python3 ## this line tells your system to use its default installation of python3

import os.path ##these lines bring in helpful libraries we use below
import requests


##### Edit the below to match your situation
dataset_dir = 'data/'  ## this is where the files will save
sheet_key = 'someVeryLongAlpha-NumericStringGoesHere4242' 
## sheet_key is revealed when you share a sheet to allow for access without a password 
## (is that ok for your data?)


                                     
sheets = { 'alpha' : 123456789, ## which tabs of the spreadsheet do you want?
        'beta' : 234567891, ## the sheets variable is a simple dictionary
        'omega' : 345678912  ## enter them here with a shortname in quotes 
        }                        ## then a colon : and the numeric gid for that sheet, 
                                 ## comma after each pair. Only one? No comma.


#### May not need to edit below here

url_base = 'https://docs.google.com/spreadsheets/d/{id}/export'.format(id=sheet_key) 
http_params = { 'format' : 'csv',   ## if you wanted something other than CSV, this is the place to change it
                 'id' : sheet_key }
table_names = sheets.keys() ##the table_names are just the part in 'quotes' from the sheets 

for table in table_names:
    print(f"Now downloading {table}....")
    http_params['gid'] = sheets[table]
    req = requests.get(url_base, params=http_params) ## <== the actual fetching is just this bit!
    output_file = open(os.path.join(dataset_dir, "%s_dataset.csv" % table), 'w') ## saving file -- filenames are the table_names you gave
    print(req.text, file=output_file)                                            ## NOTE: This will overwrite the old files!
    output_file.close()

    print("INFO: Successfully downloaded table: %s" % table)
